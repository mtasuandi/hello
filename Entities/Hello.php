<?php namespace Modules\Hello\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Hello extends Model {

  protected $fillable = ['text'];
  protected $guarded = [];
	protected $table = 'cwa_hello';

}