<?php namespace Modules\Hello\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Hello\Entities\Hello;
use GuzzleHttp\Client;

class HelloController extends Controller {
	
	public function index()
	{
		$hellos = Hello::all();

		return view('hello::index', compact( 'hellos') );
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
      'text' => 'required|max:28'
    ]);

		$hello = new Hello();
		
		$hello->text = $request->text;

		$hello->save();

		return redirect('/hello');
	}

	public function guzzle_client()
	{
		$client = new Client();
		dd($client);
	}
}