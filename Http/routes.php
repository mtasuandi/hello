<?php
/**
 * Module Hello
 */
Route::group(['prefix' => 'hello', 'namespace' => 'Modules\Hello\Http\Controllers'], function()
{
	Route::get('/', 'HelloController@index');
	Route::post('/store', 'HelloController@store');
	Route::get('/guzzle_client', 'HelloController@guzzle_client');
});