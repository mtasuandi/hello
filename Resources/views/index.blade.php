@extends('hello::layouts.master')

@section('content')
	@if (count($errors) > 0)
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  @endif

	<form action="{{ url('/hello/store') }}" method="POST">
		{!! csrf_field() !!}
		<input type="text" name="text" />
		<p>
			<input type="submit" value="Submit">
		</p>
	</form>

	<div class="data">
		@if (count($hellos) > 0)
			<table>
				<thead>
          <tr>
            <th>No</th>
            <th>Text</th>
            <th>Created at</th>
            <th>Updated at</th>
          </tr>
        </thead>
        <tbody>
        	@foreach ($hellos as $hello)
        		<tr>
        			<td>{{ $hello->id }}</td>
        			<td>{{ $hello->text }}</td>
        			<td>{{ $hello->created_at }}</td>
        			<td>{{ $hello->updated_at }}</td>
        		</tr>
					@endforeach
        </tbody>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Text</th>
            <th>Created at</th>
            <th>Updated at</th>
          </tr>
        </tfoot>
			</table>
		@endif
	</div>

	<div>
		<ul>
			<li>
				<a href="{{ url('/hello/guzzle_client') }}" target="_blank">Dependency (GuzzleHttp)</a>
			</li>
		</ul>
	</div>
@stop